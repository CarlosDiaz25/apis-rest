﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ServiceModel;
using WebAPI_Core_AppMovil.Models;

namespace WebAPI_Core_AppMovil.Interfaces
{
    [ServiceContract]
    public interface IApiSociosRestService
    {
        [OperationContract]
        public Task<Socio> ObtenerSocioAsync(int id);
    }
}
