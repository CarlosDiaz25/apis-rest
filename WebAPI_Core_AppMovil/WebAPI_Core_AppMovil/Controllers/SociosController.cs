﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAPI_Core_AppMovil.Interfaces;
using WebAPI_Core_AppMovil.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebAPI_Core_AppMovil.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class SociosController : ControllerBase
    {
        private readonly IApiSociosRestService _iapiSociosRestService;
        private Socio socio;


        public SociosController(IApiSociosRestService apiSociosRestService) => _iapiSociosRestService = apiSociosRestService;


        // GET: api/Socios/2121/45464654
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }



        // GET api/<SociosController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id, int telefono)
        {

            string coperativa = Request.Headers["COPERATIVA"];

            if (coperativa != null)
            {
                
                Respuesta resp = new Respuesta();
               
                    socio = await _iapiSociosRestService.ObtenerSocioAsync(id);

                    if (socio != null)
                    {
                        if (socio.Movil == Convert.ToString(telefono))
                        {

                            resp.status = true;
                            return Ok(resp);
                        }
                        else
                        {
                            resp.status = false;
                            return Ok(resp);
                        }
                    }
                    else
                    {
                        resp.status = false;
                        return Ok(resp);
                    }
            }
            else
            {
                return NotFound();
            }


        }
        
           


        // POST api/<SociosController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<SociosController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<SociosController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
