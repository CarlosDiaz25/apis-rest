﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WebAPI_Core_AppMovil.Interfaces;
using WebAPI_Core_AppMovil.Models;

namespace WebAPI_Core_AppMovil.Services
{
    public class ApiSociosRestService : IApiSociosRestService
    {
        public async Task<Socio> ObtenerSocioAsync(int id)
        {
            var url = "http://localhost:50703/api/v1/Socios/" + id;

            using (var http = new HttpClient())
            {
                var respose = await http.GetStringAsync(url);
                var socio = JsonConvert.DeserializeObject<Socio>(respose);
                return socio;
            }
        }
    }
}
