﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace WebAPI_Core_AppMovil.Models
{
    [DataContract]
    public class Socio
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public string Apaterno { get; set; }
        [DataMember]
        public string Amaterno { get; set; }
        [DataMember]
        public string Movil { get; set; }
    }
}
