﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APISocios.Models;
using System.Data;
using Microsoft.Data.SqlClient;
using APISocios.Common;
using Dapper;
using Microsoft.AspNetCore.JsonPatch;

namespace APISocios.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class SociosController : ControllerBase
    {
        private Socio _socio;
        private IEnumerable<Socio> _listSocio;

        public SociosController()
        {
            
        }

        // GET: api/Socios
        [HttpGet]
        public IEnumerable<Socio> GetSocio()
        {
           
            using (IDbConnection con = new SqlConnection(Global.ConnectionString))
            {
                if (con.State == ConnectionState.Closed) con.Open();

                IEnumerable<Socio> oSocios = con.Query<Socio>("SP_ObtenerSocios",
                    null,
                    commandType:CommandType.StoredProcedure);

                if(oSocios != null && oSocios.Count() > 0)
                {
                    _listSocio = oSocios;
                }
            }

                return   _listSocio;
        }

        private DynamicParameters  SetParameters(Socio oSocios, int operationType)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@id", oSocios.Id);
            parameters.Add("@nombre", oSocios.Nombre);
            parameters.Add("@Apaterno", oSocios.Apaterno );
            parameters.Add("@Amaterno", oSocios.Amaterno );
            parameters.Add("@Movil", oSocios.Movil);
            return parameters;
        }



        // GET: api/Socios/5
        [HttpGet("{id}")]
        public  async Task<ActionResult<Socio>> GetSocio(int id)
        {
            using (IDbConnection con = new SqlConnection(Global.ConnectionString))
            {
                if (con.State == ConnectionState.Closed) con.Open();

                var oSocio = con.Query<Socio>("SP_ObtenerSocio",
                    new {socioid=id},
                    commandType: CommandType.StoredProcedure);

                if (oSocio != null)
                {
                    _socio  = oSocio.SingleOrDefault();
                }
            }

            return _socio;
        }

        // PUT: api/Socios/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPatch("{id}")]
        public async Task<IActionResult> PutSocio(int id,[FromBody] JsonPatchDocument<Socio> patchEntity )
        {

            if (patchEntity == null)
            {
                return BadRequest();
            }
            using (IDbConnection con = new SqlConnection(Global.ConnectionString))
            {
                if (con.State == ConnectionState.Closed) con.Open();

                var oSocio = await con.QueryAsync<Socio>("SP_ObtenerSocio",
                     new { socioid = id },
                     commandType: CommandType.StoredProcedure);


                if (oSocio != null)
                {
                    _socio = oSocio.SingleOrDefault();
                }
                con.Close();
            }

            patchEntity.ApplyTo(_socio);

            using (IDbConnection con = new SqlConnection(Global.ConnectionString))
            {
                if (con.State == ConnectionState.Closed) con.Open();

                var oSocio = await con.QueryAsync<Socio>("SP_EditarInformacionSocio",
                     new { socioid = id,
                           movil =_socio.Movil,
                           correo=_socio.correo },
                     commandType: CommandType.StoredProcedure);

            
                con.Close();
            }


            return Ok (_socio);
        }

        //// POST: api/Socios
        //// To protect from overposting attacks, enable the specific properties you want to bind to, for
        //// more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[HttpPost]
        //public async Task<ActionResult<Socio>> PostSocio(Socio socio)
        //{
        //    _context.Socio.Add(socio);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetSocio", new { id = socio.Id }, socio);
        //}

        //// DELETE: api/Socios/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<Socio>> DeleteSocio(int id)
        //{
        //    var socio = await _context.Socio.FindAsync(id);
        //    if (socio == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Socio.Remove(socio);
        //    await _context.SaveChangesAsync();

        //    return socio;
        //}

        //private bool SocioExists(int id)
        //{
        //    return _context.Socio.Any(e => e.Id == id);
        //}
    }
}
