﻿using APISocios.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APISocios.IServices
{
    public interface ISociostService
    {
        Socio Get(int id);
    }
}
