
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE SP_EditarInformacionSocio
	@socioid INT,
	@movil VARCHAR(15),
	@correo VARCHAR(250)
AS
BEGIN
	UPDATE ss SET 
		movil=@movil,
		correo=@correo
	FROM Socio ss
	WHERE ss.id=@socioid
END
GO
