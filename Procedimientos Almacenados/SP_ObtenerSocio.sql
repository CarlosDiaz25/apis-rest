USE [AlianzaSocios]
GO
/****** Object:  StoredProcedure [dbo].[SP_ObtenerSocio]    Script Date: 9/10/2020 12:52:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SP_ObtenerSocio]
	@socioid int
AS
BEGIN
	SELECT id
		  ,nombre
		  ,apaterno
		  ,amaterno
		  ,movil
		  ,correo
	FROM dbo.Socio
	WHERE id=@socioid
END
