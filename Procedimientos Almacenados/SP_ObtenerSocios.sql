USE [AlianzaSocios]
GO
/****** Object:  StoredProcedure [dbo].[SP_ObtenerSocios]    Script Date: 9/10/2020 12:53:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SP_ObtenerSocios]

AS
BEGIN
	SELECT id
		  ,nombre
		  ,apaterno
		  ,amaterno
		  ,movil
		  ,correo
	FROM dbo.Socio
END
